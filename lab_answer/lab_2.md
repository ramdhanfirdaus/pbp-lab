#### Ramdhan Firdaus Amelia
#### 2006595753
#### Pemrograman Berbasis Platform - C
---


#### Apakah perbedaan antara JSON dan XML?

##### Perbedaan JSON dan XML
Keduanya merupakan suatu format untuk pertukaran data. Namun, keduanya memiliki beberapa perbedaan yang akan dijelaskan dalam tabel dibawah
| Parameter  | HTML                                                                         | XML                                                                                   |
|------------|------------------------------------------------------------------------------|---------------------------------------------------------------------------------------|
| Detail     | Merupakan format data                                                        | Merupakan bahasa markup                                                               |
| Bahasa     | Bagian dari sintaksis JavaScript                                             | Bagian dari SGML (Standard Generalized Markup Language)                               |
| Sintaks    | Tidak memiliki tag dan membuat sintaksnya menjadi sederhana                  | Memiliki struktur tag dan membuat rumit                                               |
| Jenis data | String, anka, boolean, dan objek, serta mendukung array                      | anyak tipe data kompleks, tetapi tidak mendukung array                                |
| Dukung UTF | UTF serta ASCII                                                              | UTF-8 dan UTF-16                                                                      |
| Keamanan   | Hampir aman setiap waktu                                                     | Rentan serangan jika perluasan entitas eksternal dan validasi DTD belum dinonaktifkan |
| Kelebihan  | Mendukung semua proser, mudah dipahami, dan sebagian besar backend mendukung | Pertukaran data dapat dilakukan dengan cepat walaupun berbeda platform                |

##### Contoh Kode JSON DAN XML 
Kode JSON  
```json
{
    "mahasiswa":[
        { 
            "npm":"2006595753",
            "nama":"Ramdhan Firdaus Amelia", 
            "jurusan":"Ilmu Komputer" 
        },

        { 
            "npm":"2002873",
            "nama":"Fathur Rizki Amelia", 
            "jurusan":"Pendidikan Olahraga" 
        },
    ]
}
```
Kode XML
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<root>
	<mahasiswa>
        <npm>2006595753</npm>
		<nama>Ramdhan Firdaus Amelia</nama>
		<jurusan>Ilmu Komputer</jurusan>
	</mahasiswa>
	<mahasiswa>
        <npm>2002873</npm>
		<nama>Fathur Rizki Amelia</nama>
		<jurusan>Pendidikan Olahraga</jurusan>
	</mahasiswa>
</root>
```


---


#### Apakah perbedaan antara HTML dan XML?

##### Perbedaan HTML dan XML?
Keduanya adalah suatu markup language. Akan tetapi, keduanya memiliki perbedaan yang mencolok, yaitu kegunaannya. HTML digunakan untuk menampilkan suatu tampilan ke web, sedangkan XML untuk menyimpan data.
Perbedaan lainnya akan dijelaskan pada tabel dibawah.
| Parameter      | HTML                                                      | XML                                                              |
|----------------|-----------------------------------------------------------|------------------------------------------------------------------|
| Sifat          | Bersifat statis                                           | Bersifat dinamis                                                 |
| Error handle   | Mengizinkan kesalahan kecil                               | Tidak mengizinkan kesalahan kecil                                |
| Case sensitive | Tidak case sensitive                                      | Case sensitive                                                   |
| Kegunaan tag   | Untuk menampilkan data                                    | Untuk menyimpan data                                             |
| Penggunaan tag | Didefinisikan developer html, boleh tidak pakai tag akhir | Diperbolehkan mendefinisikan tags sendiri, harus pakai tag akhir |
| Jumlag tag     | Karena sudah didefinisikan, jadi terbatas                 | Tidak terbatas                                                   |



##### Contoh Kode  
Kode HTML   
```html
<!DOCTYPE html>
<html>
    <head>
        <title>Ini adalah Judul</title>
    </head>
    <body>
        <h1>Ini adalah Heading</h1>
        <p>Ini adalah Paragraf.</p>
    </body>
</html>
```
Kode XML
```xml
<?xml version="1.0" encoding="UTF-8" ?>
<root>
	<mahasiswa>
        <npm>2006595753</npm>
		<nama>Ramdhan Firdaus Amelia</nama>
		<jurusan>Ilmu Komputer</jurusan>
	</mahasiswa>
	<mahasiswa>
        <npm>2002873</npm>
		<nama>Fathur Rizki Amelia</nama>
		<jurusan>Pendidikan Olahraga</jurusan>
	</mahasiswa>
</root>
```

---
## Sumber Referensi 
* https://www.monitorteknologi.com/perbedaan-json-dan-xml/
* https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
* https://www.geeksforgeeks.org/html-vs-xml/
