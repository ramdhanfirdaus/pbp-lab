from django.db import models

# Create your models here.
class Note(models.Model):
    to_user = models.CharField(max_length=200)
    from_user = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    message = models.TextField()