import 'package:flutter/material.dart';
import 'package:pbp_c07/dummy_data.dart';
import 'package:pbp_c07/models/masker.dart';
import 'package:pbp_c07/utils/ui_helper.dart';
import 'package:pbp_c07/widgets/main_drawer.dart';
import 'package:pbp_c07/widgets/masker_list_item_view.dart';

class ProductScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<ProductScreen> with SingleTickerProviderStateMixin {
  TabController? _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('All of C07 Masker'),
        backgroundColor: Colors.black87,
      ),
      drawer: MainDrawer(),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              UIHelper.verticalSpaceMedium(),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    _ProductListView(),
                    _ProductListView(),
                      ],
                    ),
                  ),
                ]
              )
          ),
        ),
    );
  }
}

class _ProductListView extends StatelessWidget {
  final List<Masker> masker = Masker.getMasker();

  @override
  Widget build(BuildContext context) {
    masker.addAll(DUMMY_MASKER);
    masker.shuffle();

    return ListView.builder(
      shrinkWrap: true,
      itemCount: masker.length,
      itemBuilder: (context, index) => MaskerListItemView(
        masker: masker[index],
      ),
    );
  }
}
